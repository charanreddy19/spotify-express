const express=require('express')
const app=express()
const path=require('path')
const sql=require('mysql')
const cors=require('cors')
const { response } = require('express')
app.use(cors())

require("dotenv").config();

const connection = sql.createPool({
    host: process.env.host,
    database: process.env.database,
    user: process.env.user,
    password: process.env.password,
});


app.use(express.json())

app.delete('/deletePlaylist',(req,res)=>{
    let name=req.query.name
    connection.query(`drop table playlist_${name}`,(err,results)=>{
        res.json(results)
    })
})

app.put('/addsongtoplaylist',(req,res)=>{
    let id=req.query.id
    let name=req.query.name
    connection.query(`insert into playlist_${name}(song_id) values(${id})`,(err,results)=>{
        res.json(results)
    })
})

app.put(`/removePlaylist`,(req,res)=>{
    let name=req.query.name
    connection.query(`delete from playlists where name="${name}"`,(err,results)=>{
        res.json(results)
    })
})

app.post('/playlist',(req,res)=>{
    let name=req.query.name
    connection.query(`create table if not exists playlist_${name}(
        id int primary key auto_increment,
        song_id int references songs(id));`,(err,results)=>{
        if(err){
            res.json({msg:err})
        }
        else {
            res.json(results)
        }
    })
})

app.post('/insertPlaylist',(req,res)=>{
    let name=req.query.name
    connection.query(`insert into playlists(name) values('${name}')`,(err,results)=>{
        if(err){
            res.json({msg:err})
        }
        else {
            res.json(results)
        }
    })
})

app.get('/playlistsongs',(req,res)=>{
    let name=req.query.name
    connection.query(`select * from playlist_${name}`,(err,results)=>{
        res.json(results)
    })
})

app.get('/playlists',(req,res)=>{
    connection.query('select * from playlists limit 5',(err,results)=>{
        res.json(results)
    })
})

app.get('/allplaylists',(req,res)=>{
    connection.query('select * from playlists',(err,results)=>{
        res.json(results)
    })
})


app.get('/songs',(req,res)=>{
    connection.query('select * from songs',(err,results)=>{
        res.json(results)
    })
})

app.get('/recentPlayed',(req,res)=>{
    connection.query('select * from recent_played order by id desc',(err,results)=>{
        console.log(results)
        res.json(results)
    })
})

app.get('/recommended',(req,res)=>{
    connection.query('select * from recent_played limit 5',(err,results)=>{
        console.log(results)
        res.json(results)
    })
})

app.get('/allrecommended',(req,res)=>{
    connection.query('select * from recent_played',(err,results)=>{
        console.log(results)
        res.json(results)
    })
})

app.get('/recent',(req,res)=>{
    connection.query('select * from recent_played order by id desc limit 1',(err,results)=>{
        res.json(results)
    })
})


app.get('/firstFiveRecent',(req,res)=>{
    connection.query(`select * from recent_played order by id desc limit 5`,(err,results)=>{
        res.json(results)
    })
})

app.get('/firstFive',(req,res)=>{
    connection.query('select * from songs limit 5',(err,results)=>{
        res.json(results)
    })
})

app.get('/searchSong',(req,res)=>{
    let string=req.query.name
    connection.query(`select * from songs where name like "%${string}%"`,(err,results)=>{
        res.json(results)
    })
})

app.get('/searchArtist',(req,res)=>{
    let string=req.query.name
    connection.query(`select * from songs where artist_name like "%${string}%"`,(err,results)=>{
        res.json(results)
    })
})

app.get('/artists',(req,res)=>{
    connection.query('select distinct artist_name from songs',(err,results)=>{
        res.json(results)
    })
})

app.get('/:id',(req,res)=>{
    let id=req.params.id
    connection.query(`select * from songs where id=${id}`,(err,results)=>{
        res.json(results)
    })
})

app.get('/likedSongs',(req,res)=>{
    connection.query('select * from liked_songs',(err,results)=>{
        res.json(results)
        res.end()
    })
})

app.get('/playlist',(req,res)=>{
    let name=req.query.name
    connection.query(`select * from playlist_${name}`,(err,results)=>{
        if(err){
            res.json({msg:err})
        }
        else {
            res.json(results)
        }
    })
})

app.post('/recentPlayed',(req,res)=>{
    let id=req.query.id
    console.log(id)
    connection.query(`insert into recent_played (song_id) values(${id})`,(err,results)=>{
        if(err){
            res.json({msg:err})
        }
        else {
            res.json(results)
        }
    })
})

app.post('/likedSongs',(req,res)=>{
    let id=req.query.id
    connection.query(`insert into liked_songs (song_id) values(${id})`,(err,results)=>{
        if(err){
            res.json({msg:err})
        }
        else {
            res.json(results)
        }
    })
})


app.post('/signup',(req,res)=>{
    let name=req.query.name
    let password=req.query.password
    let profileName=req.query.profileName
    let dateOfBirth=req.query.dateOfBirth
    let gender=req.query.gender
    connection.query(`insert into users (username,password,profileName,dateOfBirth,gender) values('${name}','${password}','${profileName}','${dateOfBirth}','${gender}')`,(error,results)=>{
        if(error) {
            res.json({mag:err})
        }
        else {
            res.json(results)
        }
    })
})



app.listen(process.env.PORT || 8000,()=>{
    console.log("listening....")
})