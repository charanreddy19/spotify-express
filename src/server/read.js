const fs = require("fs");
const path = require("path");

const sql = require("mysql");
require("dotenv").config();

const connection = sql.createPool({
    host: process.env.host,
    database: process.env.database,
    user: process.env.user,
    password: process.env.password,
});

fs.readFile(path.join(__dirname,'../../spotify.json'), "utf8", (err, results) => {
    let data = JSON.parse(results);
    console.log(data)
    //console.log(data['tracks']['items'][25])
    //console.log(data['tracks']['items'][0]['track']['name'])   //name
    // console.log(data['tracks']['items'][0]['track']['artists'])   //-----array of objects
    //console.log(data['tracks']['items'][0]['track']['artists'][0]['name'])  //artist name
    // console.log(data['tracks']['items'][0]['track']['album']['images'])  //image
    //console.log(data['tracks']['items'][0]['track']['preview_url'])    //preview_url
    // console.log(data['tracks']['items'][0]['track']['album']['name'])  //album-name
});

function query(values) {
    return new Promise((resolve, reject) => {
        connection.query(
            "insert into songs (name,artist_name,image_url,url,album_name) values ?",
            [values],
            (err, results) => {
                if (err) {
                    console.log(err);
                }
                console.log(results);
            }
        );
    });
}

fs.readFile("./spotify.json", "utf8", (err, results) => {
    let data = JSON.parse(results);
    let arr = [];
    console.log(data['tracks']['items'].length)
    for (let song of data["tracks"]["items"]) {
        name = song["track"]["name"];
        artist = song["track"]["artists"][0]["name"];
        image = song["track"]["album"]["images"][0]["url"];
        url = song["track"]["preview_url"];
        album = song["track"]["album"]["name"];
        arr.push([name, artist, image, url, album]);
    }
    query(arr);
});